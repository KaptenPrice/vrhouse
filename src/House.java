class House implements HouseUtilities {
    private boolean home = false;
    private boolean plansTomorrow = false;

    public boolean GetPlansTomorrow() {
        return plansTomorrow;
    }

    public void setPlansTomorrow(boolean plansTomorrow) {
        this.plansTomorrow = plansTomorrow;
    }

    public boolean getHome() {
        return this.home;
    }


    public void setHome(boolean home) {
        this.home = home;
    }


    @Override
    public String lampStatus(boolean isHome) {
        return null;
    }

    @Override
    public boolean isHome() {
        return false;
    }

    @Override
    public boolean plansTomorrow() {
        return false;
    }

    @Override
    public String varning(boolean isHome) {
        return null;
    }

    @Override
    public String wakeUp(boolean plansTomorrow) {
        return null;
    }


}