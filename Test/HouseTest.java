import org.junit.*;

public class HouseTest {




    @Test
    public void isHomeTest() {
        House house = new House();
        house.setHome(house.getHome());
        Assert.assertEquals(true,house.isHome());

    }
    @Test
    public void lampStatus() {
        House house=new House();
        house.lampStatus(house.getHome());
        Assert.assertEquals("lights on",true,house.lampStatus(house.getHome()));

    }

    @Test
    public void varning() {
        House house=new House();
        house.varning(house.getHome());
        Assert.assertEquals("Fridge is closed",true,house.varning(house.getHome()));

    }

    @Test
    public void wakeUpTest() {
        House house=new House();
        house.wakeUp(house.GetPlansTomorrow());
        Assert.assertEquals("No wake up",true,house.GetPlansTomorrow());

    }

    @Test
    public void plansTomorrowTest() {
        House house=new House();
        house.setPlansTomorrow(house.GetPlansTomorrow());
        Assert.assertEquals("School tomorrow",true,house.GetPlansTomorrow());
    }
}